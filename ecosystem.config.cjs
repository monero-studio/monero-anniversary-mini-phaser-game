module.exports = {
  apps: [
    {
      name: "moneroof-wall",
      script: "npm",
      args: "run start",
      env: {
        NODE_ENV: "production",
        PORT: 1234,
      },
    },
  ],
};
