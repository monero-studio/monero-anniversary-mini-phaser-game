import Phaser from "phaser";

let numTxRecieved = 0;
let amountTxReceived = 0;
let totalAmountOfCoins = 0;

const statStyle: Phaser.Types.GameObjects.Text.TextStyle = {
  fontSize: "32px",
  backgroundColor: "#4c4c4c",
  color: "#fefdff",
  padding: {
    x: 10,
    y: 5,
  },
};

export default class GameScene extends Phaser.Scene {
  constructor() {
    super({ key: "GameScene" });
  }

  preload() {
    // load bg images for 8 large screens
    for (let i = 0; i < 8; i++) {
      this.load.image(`image-${i}`, `assets/image-${i}.png`);
    }

    this.load.image("astronaut", "assets/astronaut.png");
    this.load.image("coin", "assets/monero-small.png");
    this.load.image("logo", "assets/monero-logo.png");
  }

  create() {
    this.matter.world.setBounds(
      0,
      0,
      this.sys.canvas.width,
      this.sys.canvas.height,
      32,
      true,
      true,
      false,
      true,
    );

    // rendering the bg images
    for (let i = 0; i < 8; i++) {
      this.add
        .image(i * this.textures.getFrame(`image-${i}`).width, 0, `image-${i}`)
        .setOrigin(0, 0);
    }

    // this.matter.add.circle(1150, 575, 260, {
    //   isStatic: true,
    // });

    this.matter.add.circle(3450, 788, 222, {
      isStatic: true,
    });

    // this.matter.add.circle(4963, 1040, 210, {
    //   isStatic: true,
    // });

    this.add.text(
      this.sys.canvas.width - this.sys.canvas.width / 8 + 16,
      16,
      `💸 Pricing in XMR:\n- 0.0001-0.0008: 10-20 🪙\n- 0.0008-0.008: 20-40 🪙\n- 0.008-0.04: 50-100 🪙\n- >0.04: 200-400 🪙`,
      statStyle,
    );

    const statusText = this.add.text(16, 16, `🔄 Connecting...`, statStyle);

    const heightText = this.add.text(16, 64, `📏 Height: 0`, statStyle);

    const numberOfTxText = this.add.text(
      16,
      112,
      `🎫 Received TXs: ${numTxRecieved}`,
      statStyle,
    );

    const amountOfTxText = this.add.text(
      16,
      160,
      `💰 Amount: ${amountTxReceived} XMR`,
      statStyle,
    );

    const amountOfCoinsText = this.add.text(
      16,
      208,
      `🪙 Coins: ${amountTxReceived}`,
      statStyle,
    );

    this.events.on(
      "new-block",
      (data?: { height: number; syncDone: boolean }) => {
        if (!data) {
          console.log("No data in new-block", { data });
          return;
        }
        const { height, syncDone } = data;
        heightText.setText(`📏 Height: ${height}`);
        statusText.setText(syncDone ? `✅ Synced` : `⏳ Syncing`);
      },
    );

    // trigger coin-fetti rain
    this.events.on("new-tx", (data?: { amount: number }) => {
      if (!data) {
        // if the game loads, data is not there yet
        console.log("No data in new-tx", { data });
        return;
      }

      const { amount } = data;

      let amountModifier;
      if (amount <= 0.0001) amountModifier = 10;
      else if (amount > 0.0001 && amount <= 0.0008) amountModifier = 20;
      else if (amount > 0.0008 && amount <= 0.008) amountModifier = 50;
      else if (amount > 0.008 && amount <= 0.04) amountModifier = 100;
      else if (amount > 0.04) amountModifier = 200;
      const numberOfCoins = Phaser.Math.Between(
        amountModifier,
        amountModifier * 2,
      );

      totalAmountOfCoins += numberOfCoins;

      amountTxReceived += amount;
      numTxRecieved += 1;

      amountOfTxText.setText(`💰 Amount: ${amountTxReceived} XMR`);
      numberOfTxText.setText(`🎫 Received TXs: ${numTxRecieved}`);

      amountOfCoinsText.setText(`🪙 Coins: ${totalAmountOfCoins}`);

      for (let i = 0; i < numberOfCoins; i++) {
        const coin = this.matter.add.image(
          Phaser.Math.Between(0, this.sys.canvas.width),
          Phaser.Math.Between(-600, 0),
          "coin",
        );

        coin.setCircle(coin.displayWidth / 2);
        coin.setScale(
          Phaser.Math.FloatBetween(0.6, Phaser.Math.Clamp(1 + amount, 1, 1.4)),
        );
        coin.setAngularVelocity(Phaser.Math.FloatBetween(-3, 3));
        coin.setVelocityX(Phaser.Math.Between(-20, 20));
        coin.setVelocityY(Phaser.Math.Between(-10, 10));
        coin.setFriction(1);
        coin.setBounce(1);
      }
    });
  }
}
