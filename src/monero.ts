import * as moneroTs from "monero-ts";

import { START_RESTORE_HEIGHT, game } from "./game.js";

// NOTE very simple in-memory store to only let every tx count once
// 💡 tx are seen twice by the wallet:
// once when unconfirmed, and once when confirmed
export const seenTransactionHashes: string[] = [];

export let blockHeight = 0;

export async function initWallet(
  network:
    | typeof moneroTs.MoneroNetworkType.STAGENET
    | typeof moneroTs.MoneroNetworkType.MAINNET,
) {
  // NOTE just call this once to get an open wallet with the listener already set up
  let wallet: moneroTs.MoneroWalletFull;
  switch (network) {
    case moneroTs.MoneroNetworkType.STAGENET:
      wallet = await moneroTs.createWalletFull({
        privateViewKey:
          "91ab0d94e7da9b272adc0ce80dbadca6e3c2bbfe222650b68c6d6968f8fedd06",
        primaryAddress:
          "54MUTaYMgnaLn5dD8ejehd4bi8YqkvENR1H9FQsb8zS9bWM3XLfXMevaVNSPh9kZ6RA2GR8roqcoYRdPHMHwHW2wAJ8aNc7",
        server: {
          uri: "stagenet.community.rino.io:38081",
        },
        restoreHeight: START_RESTORE_HEIGHT,
        networkType: moneroTs.MoneroNetworkType.STAGENET,
      });
      break;
    case moneroTs.MoneroNetworkType.MAINNET:
      // this is a view-only wallet. You can see incoming transactions, but not spend them.
      // secrets like the privateSpendKey should NEVER be commited to a repository
      // this is a view-only wallet. So I don't care.
      wallet = await moneroTs.createWalletFull({
        privateViewKey:
          "1a5f11b4705b7412c109cab0822711b991b160c832f9c7e67325b7d29e0c720e",
        primaryAddress:
          "44P7zbhj4Hf7CaVnq2p3HPWHM629PiktnfofXmgfU5kA2csc3isAdiGE8uA7ynJFefbctTm72Uq2EB7GK4SDc8qy3ntoYED",
        server: {
          uri: "xmr.lolfox.au:443",
          rejectUnauthorized: false,
        },
        restoreHeight: START_RESTORE_HEIGHT,
        networkType: moneroTs.MoneroNetworkType.MAINNET,
      });
      break;
  }

  await wallet.addListener(
    // NOTE this listener fires everytime our monero-wallet-rpc server sees a new transaction
    new (class extends moneroTs.MoneroWalletListener {
      async onOutputReceived(output: moneroTs.MoneroOutputWallet) {
        // parse the transaction
        const amount = convertBigIntToXmrFloat(output.getAmount());
        const transactionHash = output.getTx().getHash();

        if (!seenTransactionHashes.includes(transactionHash)) {
          // if it's the first time we see the tx, we trigger the COIN-FETTI RAIN 🥳🎉
          triggerNewTxEvent(amount);
          seenTransactionHashes.push(transactionHash);
        }

        // NOTE saving the wallet to file to store the tx on file
        await wallet.save();
      }

      async onSyncProgress(
        height: number,
        startHeight: number,
        endHeight: number,
        percentDone: number,
        message: string,
      ) {
        triggerNewBlockEvent({ height, percentDone });
      }
    })(),
  );

  wallet.startSyncing();
  console.info("Server wallet initialized");

  return wallet;
}

function convertBigIntToXmrFloat(amount: bigint) {
  // NOTE the output of a monero transaction is a big int, wtf is that even
  return parseFloat((Number(amount) / 1000000000000).toFixed(12));
}

export function triggerNewBlockEvent({
  height,
  percentDone,
}: {
  height: number;
  percentDone: number;
}) {
  // get the phaser scene and fire the COIN-FETTI RAIN 🥳🎉
  const scene = game.scene.getScene("GameScene");
  scene.events.emit("new-block", { height, syncDone: percentDone === 1 });
}

export function triggerNewTxEvent(amount: number) {
  // get the phaser scene and fire the COIN-FETTI RAIN 🥳🎉
  const scene = game.scene.getScene("GameScene");
  scene.events.emit("new-tx", { amount });
}

export function updateBlockHeightText(
  textObject: Phaser.GameObjects.Text,
  height: number,
) {
  textObject.setText(`Height: ${height}`);
}
