import Phaser from "phaser";

import GameScene from "./game-scene.js";
import { initWallet } from "./monero.js";
import moneroTs from "monero-ts";

// some important settings
export const START_RESTORE_HEIGHT = 3254027;
const NETWORK_TYPE = moneroTs.MoneroNetworkType.MAINNET;

export const phaserConfig: Phaser.Types.Core.GameConfig = {
  parent: "game",
  scale: {
    width: 6144,
    height: 1366,
    mode: Phaser.Scale.ScaleModes.FIT,
    autoCenter: Phaser.Scale.Center.CENTER_BOTH,
  },
  expandParent: true,

  fullscreenTarget: "game",

  transparent: true,
  physics: {
    default: "matter",
    matter: {
      debug: false,
      gravity: {
        x: 0,
        y: 0.4,
      },
    },
  },
  disableContextMenu: true,

  scene: [GameScene],
};

// setInterval(() => {
//   triggerNewTxEvent(0.01);
// }, 2000);

// setTimeout(() => triggerNewTxEvent(0.01), 2000);

export const game = new Phaser.Game(phaserConfig);

(async () => {
  const wallet = await initWallet(NETWORK_TYPE);
  const primaryAddress = await wallet.getPrimaryAddress();
  console.info(
    `The primary address of the server wallet is: ${primaryAddress}`,
  );
})();
